from django.urls import path, include
from django.conf.urls import url
from . import views

#NOTE DECLARACIÓN DE UN NOMBRE DE APLICACION 
#PARA QUE LAS VISTAS PUEDAN SER ACCEDIDAS DESDE CUALQUIER TEMPLATE

app_name ="Posts"


urlpatterns = [
    path('', views.post_list, name='list'),
    path('home/', views.post_home),
    path('create/', views.post_create, name='create_post'),
    path('<slug:slug>/', views.post_detail, name='detail'),
    path('detail/autor/<int:id>', views.post_detail_autor, name='detail_autor'),
    #url(r'^(?P<slug>[\w-]+)/$', views.post_detail, name='detail'),
    path('<slug:slug>/update/', views.post_update, name='update'),
    path('<slug:slug>/delete/', views.post_delete, name='delete'),
    path('create/category/', views.category_create, name='create_category'),
    path('create/autor/', views.registrar_autor, name='create_autor'),
    path('list/autor', views.list_autor, name='list_autor')
]


#ANCHOR USO DE DE LA FUNCION URL(), EJEMPLO DEL USO DE "SLUG" 
#REVIEW url(r'^(?P<slug>[\w-]+)/$', views.post_detail, name='detail'),
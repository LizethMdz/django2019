from urllib.parse import quote_plus #Decode o Uncode cadena de texto para URL
from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, HttpResponseRedirect, Http404
from .models import Post, Post_Autor, Post_categoria
from .forms import PostForm, Post_CategoriasForm, Post_AutorForm
from django.contrib import messages
from django.utils import timezone
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q #Libreria para realizar varios filtros

# Create your views here.
def post_home(request):
    #Return string 
    return HttpResponse("<h1>prueba</h1>")

def post_create(request):
    if not request.user.is_staff or not request.user.is_superuser:
        raise Http404
    form = PostForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        messages.success(request, "Haz creado un nuevo Post :D")
        return HttpResponseRedirect(instance.get_absolute_url())
    context = {
        "form" : form,
        "titlepage" : "Create a new post"
    }
    return render(request, "post_form.html", context)

def post_detail(request, slug=None):
    #instance = Post.objects.get(id)
    instance = get_object_or_404(Post, slug=slug)
    if instance.publish > timezone.now().date() or instance.draft:
        if not request.user.is_staff or not request.user.is_superuser:
            raise Http404
    share_string = quote_plus(instance.titulo)
    context = {
        "titulo" : instance.titulo,
        "elemento" : instance,
        "share_string": share_string,
    }
    return render(request, "detail.html", context)

def post_list(request):
    hoy = timezone.now().date()
    #NOTE Se le colocó .ACTIVE debido a que se sobreescribió el metodo ALL() con POSTMANAGER
    queryset_list = Post.objects.active()#filter(draft = False).filter(publish__lte=timezone.now())#all().order_by('-timestamp')
    if request.user.is_staff or request.user.is_superuser:
        queryset_list = Post.objects.all()

    query_search = request.GET.get("q")

    if query_search:
        queryset_list = queryset_list.filter(
            Q(titulo__contains=query_search)|
            Q(contenido__contains=query_search)|
            Q(user__first_name__contains=query_search)|
            Q(user__last_name__contains=query_search)
            ).distinct()
    
    paginator = Paginator(queryset_list, 5) # Show 25 contacts per page
    page = request.GET.get('page')

    try:
        queryset = paginator.get_page(page)
    except PageNotAnInteger:
        queryset = paginator.page(1)
    except EmptyPage:
        queryset = paginator.page(paginator.num_pages)
    context = {
        "titulo" : "Principal",
        "publicaciones" : queryset,
        "hoy": hoy,
    }
    return render(request, "post_list.html", context)

def post_update(request, slug=None):
    if not request.user.is_staff or not request.user.is_superuser:
        raise Http404
    instance = get_object_or_404(Post, slug=slug)
    form = PostForm(request.POST or None, request.FILES or None, instance=instance)
    if request.method == "POST":
        instance = form.save(commit=False)
        instance.save()
        messages.success(request, "Haz editado un nuevo Post :D")
        return HttpResponseRedirect(instance.get_absolute_url())
    context = {
        "titulo" : instance.titulo,
        "elemento" : instance,
        "form" : form,
    }
    return render(request, "post_form.html", context)

def post_delete(request, slug=None):
    if not request.user.is_staff or not request.user.is_superuser:
        raise Http404
    instance = get_object_or_404(Post, slug=slug)
    instance.delete()
    messages.success(request, "Tu post ha sido borrado exitosamente")
    #Regresa a la página 
    return redirect("list")

def category_create(request):
    form_categoria = Post_CategoriasForm(request.POST or None)
    if request.method == "POST":
        instance = form_categoria.save(commit=False)
        instance.save()
        messages.success(request, "Ha sido creada")
    context = {
        "form_categoria" : form_categoria,
        "titlepage" : "Crea una categoría"
    }
    return render(request, "category_form.html", context)
    
    return HttpResponseRedirect(instance.get_absolute_url())

def registrar_autor(request):
    form_autores = Post_AutorForm(request.POST or None, request.FILES or None)
    if request.method == "POST":
        instance = form_autores.save(commit=False)
        instance.save() 
        messages.success(request, "Ha sido creado")
        return HttpResponseRedirect(instance.get_absolute_url_2())
    context = {
        "form_autores": form_autores,
        "title_page" : "Registrar autor"
    }
    return render(request, "autor_form.html", context)

def post_detail_autor(request, id=None):
    
    instance = get_object_or_404(Post_Autor, id=id)
    if not request.user.is_staff or not request.user.is_superuser:
        raise Http404
    
    context = {
        "nombre" : instance.nombre,
        "autor_" : instance,
    }
    return render(request, "detail_autor.html", context)


def list_autor(request):
    queryset_list = Post_Autor.objects.all()
    queryset = Paginator(queryset_list, 5) # Show 25 contacts per page
    page = request.GET.get('page')
    queryset = queryset.get_page(page)
    context = {
        "titulo" : "Autores",
        "autores" : queryset,
    }
    return render(request, "autor_list.html", context)


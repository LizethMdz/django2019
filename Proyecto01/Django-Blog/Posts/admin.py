from django.contrib import admin
from .models import Post, Post_Autor, Post_categoria
# Register your models here.
class PostModelAdmin(admin.ModelAdmin):
    #Personaliza nuestra visualización de campos
    list_display = ["titulo", "timestamp", "actualizado"]
    #Personaliza los links list_display_links
    list_display_links = ["timestamp"]
    #Personaliza el filtro de los elementos dependiendo el campo
    list_filter = ["titulo"]
    #Personaliza para que se pueda hacer una busqueda
    search_fields = ["titulo", "contenido"]
    #Personaliza que el campo que se coloca es editable
    #Pero su list_display_links debe ser otro campo
    list_editable = ["titulo"]
    class Meta:
        model = Post

admin.site.register(Post, PostModelAdmin)


class PostAutorModelAdmin(admin.ModelAdmin):
    class Meta:
        model = Post_Autor

admin.site.register(Post_Autor, PostAutorModelAdmin)


class PostCategoriaModelAdmin(admin.ModelAdmin):
    class Meta:
        model = Post_categoria

admin.site.register(Post_categoria, PostCategoriaModelAdmin)


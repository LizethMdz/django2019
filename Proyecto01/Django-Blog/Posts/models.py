from __future__ import unicode_literals
from django.db import models
from django.urls import reverse #Cambio django.core.urlreverse
from django.db.models.signals import pre_save
from django.utils.text import slugify
from django.conf import settings
from django.utils import timezone
# Create your models here.
app_name = "Posts"

#NOTE Sobrescritura del metodo ALL()
class PostManager(models.Manager):
    def active(self, *args, **kwargs):
        return super(PostManager, self).filter(draft=False).filter(publish__lte=timezone.now())

def upload_location(instance, filename):
    return "Posts/%d/%s" %(instance.id, filename)

def upload_location_2(instance, filename):
    return "Avatars/%d_%s/%s" %(instance.id, instance.ap_mat, filename)

class Post(models.Model):
    """docstring for Post."""
    user = models.ForeignKey(settings.AUTH_USER_MODEL, default=1,  on_delete=models.CASCADE)
    titulo = models.CharField(max_length=150)
    slug = models.SlugField(unique=True)
    #imagen = models.FileField(null=True, blank=True) #De esta manera trabajamos con imagenes y las almacenamos tal cual en el directorio declarado
    imagen = models.ImageField(upload_to=upload_location,
    null=True,
    blank=True,
    height_field="height_field",
    width_field="width_field") #Esta es la mejor forma de guardar las imagenes dentro de un directorio ya declarado
    height_field = models.IntegerField(default = 0)
    width_field = models.IntegerField(default = 0)
    contenido = models.TextField()
    draft = models.BooleanField(default=False)
    publish = models.DateField(auto_now=False, auto_now_add=False) #Fecha de publicacion establecida manualmente
    timestamp = models.DateTimeField(auto_now_add=True, auto_now = False)
    actualizado = models.DateTimeField(auto_now_add=False, auto_now=True)


    objects = PostManager()

    def __str__(self):
        return self.titulo
        
    # **Se agregó una linea de codigo en index
    def get_absolute_url(self):
        #return "/posts/%s/" %(self.slug)
        return reverse("Posts:detail", kwargs={"slug": self.slug})

class Post_Autor(models.Model):
    nombre = models.CharField(max_length=150, null=True)
    ap_pat = models.CharField(max_length=50, null=True)
    ap_mat = models.CharField(max_length=50, null=True)
    imagen_autor = models.ImageField(null=True, upload_to=upload_location_2,
    blank=True,
    height_field="height_field",
    width_field="width_field",
    default = 'Avatars/no-img.png')
    height_field = models.IntegerField(default = 0)
    width_field = models.IntegerField(default = 0)
    timestamp = models.DateTimeField(auto_now_add= True, auto_now = False)

    def __str__(self):
        return self.nombre

    def get_absolute_url_2(self):
        return reverse("Posts:detail_autor", kwargs={"id": self.id})

class Post_categoria(models.Model):
    nombre = models.CharField(max_length=100)

    def __str__(self):
        return self.nombre

    # **Se agregó una linea de codigo en index
    def get_absolute_url(self):
        return self
        #return "/posts/category/%s/" %(self.slug)
        #return reverse("Posts:detail", kwargs={"slug": self.slug})

def create_slug(instance, new_slug=None):
    slug = slugify(instance.titulo)
    if new_slug is not None:
        slug = new_slug
    qs = Post.objects.filter(slug=slug).order_by("-id")
    exists = qs.exists()
    if exists:
        new_slug = "%s-%s" %(slug, qs.first().id)
        return create_slug(instance, new_slug = new_slug)
    return slug

def pre_save_post_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = create_slug(instance)

pre_save.connect(pre_save_post_receiver, sender=Post)

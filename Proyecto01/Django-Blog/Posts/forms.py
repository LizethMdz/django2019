from django import forms
from .models import Post,  Post_Autor, Post_categoria

class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = [
            "titulo",
            "contenido",
            "imagen",
            "draft",
            "publish",
        ]

        labels = {
            'titulo': 'Titulo de Publicación',
            'contenido': 'Contenido de Publicación', 
        }
        
class Post_AutorForm(forms.ModelForm):
    class Meta:
        model = Post_Autor
        fields = [
            "nombre",
            "ap_pat",
            "ap_mat",
            "imagen_autor",
        ]
        labels = {
            'nombre': 'Nombre(s)',
            'ap_pat': 'Apellido Paterno',
            'ap_mat': 'Apellido Materno',
            'imagen_autor': 'Avatar', 
        }


class Post_CategoriasForm(forms.ModelForm):
    class Meta: 
        model = Post_categoria
        fields = [
            "nombre",
        ]


